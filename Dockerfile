# Use uma imagem base com Python
FROM python:3.9-slim

# Defina o diretório de trabalho
WORKDIR /app

# Copie os arquivos de script para o contêiner
COPY notificacaoFalha.sh /app/notificacaoFalha.sh
COPY notificacaoSucesso.sh /app/notificacaoSucesso.sh
COPY plot_simples_grafico.py /app/plot_simples_grafico.py
COPY relogio.py /app/relogio.py

# Dê permissão de execução aos scripts bash
RUN chmod +x /app/notificacaoFalha.sh
RUN chmod +x /app/notificacaoSucesso.sh

# Instale dependências necessárias (exemplo: matplotlib para plot_simples_grafico.py)
RUN pip install matplotlib

# Comando padrão ao iniciar o contêiner
CMD ["python", "relogio.py"]

# Para executar scripts bash como comando padrão (opcional)
# CMD ["/app/notificacaoSucesso.sh"]